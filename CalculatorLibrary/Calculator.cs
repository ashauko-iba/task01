﻿using System;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace CalculatorLibrary
{
    public class Calculator
    {
        /// <summary>
        /// Use this method to get the result of an expression.
        /// </summary>
        /// <param name="s">Incoming string with some expression</param>
        /// <returns>Calculation result</returns>
        public static BigInteger Calculate(string s)
        {
            return GetResult(s);
        }

        private static BigInteger GetResult(string s)
        {
            try
            {
                return CalculatorLogic.GetResult(s = s.Replace(" ", ""));
            }
            catch(DivideByZeroException ex)
            {
                throw ex;
            }
            catch(FormatException ex)
            {
                throw ex;
            }
            catch(ArgumentOutOfRangeException ex)
            {
                throw ex;
            }
            catch
            {
                throw GetException(s);
            }
        }

        private static Exception GetException(string s)
        {
            //check some exceptions with spaces or clean string
            if ((s.Length == 0) || (s.Count(x => x == ' ') == s.Length))
                throw new FormatException("Нарушение синтаксиса выражения: Поступила пустая или состоящая из пробелов строка.");
                //ExceptionMessage = "Нарушение синтаксиса выражения: Поступила пустая или состоящая из пробелов строка.";

            //check the number of brackets
            if (s.Count(x => x == '(') != s.Count(x => x == ')'))
                throw new FormatException("Нарушение синтаксиса выражения: Количество открытых скобок не равняется количеству закрытых.");

            //any symbols != {1234567890+-*/ }
            {
                char[] AvailableChars = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ' ', '+', '-', '*', '/' , '(', ')'};
                foreach (var item in s)
                {
                    if (!AvailableChars.Contains(item))
                        throw new FormatException("Нарушение синтаксиса выражения: Входное выражение содержит посторонние символы.");
                }
            }

            return new Exception("Неопознанная проблема при обработке выражения.Проверьте корректность введённых данных.");
        }
    }
}
