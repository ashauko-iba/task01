﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorLibrary
{
    public class CalculatorLogic
    {
        public static Stack<BigInteger> answerStack = new Stack<BigInteger>();

        public static BigInteger GetResult(string s)
        {
            try
            {
                if (s.Length > 1000000)
                    throw new ArgumentOutOfRangeException("Размер входного выражения вышел за пределы (1млн символов)");
                List<string> tmp = GetList(s);
                foreach (var item in tmp)
                {
                    StackWorker(item);
                }
                return answerStack.Pop();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        //Создание стека на основе польской записи строкового выражения
        private static List<string> GetList(string s)
        {
            Stack<char> operationsStack = new Stack<char>();

            char lastOperation;

            string resultString = string.Empty;
            s = s.Replace(" ", "");

            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsDigit(s[i]))
                {
                    resultString += s[i];
                    continue;
                }

                if (IsOperation(s[i]))
                {
                    resultString += "|";
                    if (!(operationsStack.Count == 0))
                        lastOperation = operationsStack.Peek();
                    else
                    {
                        operationsStack.Push(s[i]);
                        continue;
                    }
                    if (GetOperationPriority(lastOperation) < GetOperationPriority(s[i]))
                    {
                        operationsStack.Push(s[i]);
                        continue;
                    }
                    else
                    {
                        resultString += "|" + operationsStack.Pop() + "|";
                        operationsStack.Push(s[i]);
                        continue;
                    }
                }
                if (s[i].Equals('('))
                {
                    operationsStack.Push(s[i]);
                    continue;
                }
                if (s[i].Equals(')'))
                {
                    while (operationsStack.Peek() != '(')
                    {
                        resultString += "|" + operationsStack.Pop() + "|";
                    }
                    operationsStack.Pop();
                }
            }
            while (!(operationsStack.Count == 0))
            {
                resultString += "|" + operationsStack.Pop() + "|";
            }

            List<string> res = resultString.Split('|').ToList();
            res.RemoveAll(string.IsNullOrEmpty);
            return res;
        }

        //Чекер на символ операции
        private static bool IsOperation(char c)
        {
            if (c == '+' || c == '-' || c == '*' || c == '/')
                return true;
            else
                return false;
        }

        //Возвращает приоритет операции
        private static int GetOperationPriority(char c)
        {
            switch (c)
            {
                case '+': return 1;
                case '-': return 1;
                case '*': return 2;
                case '/': return 2;
                default: return 0;
            }
        }

        //Поэтапное вычисление выражения, основанного на обратной польской записи
        //Метод вызывается в цикле с последовательной передачей числа или оператора
        private static void StackWorker(string arg)
        {
            BigInteger num = new BigInteger();
            if (BigInteger.TryParse(arg, out  num))
                answerStack.Push(num);
            else
            {
                BigInteger num2;
                switch (arg)
                {
                    case "+":
                        answerStack.Push(answerStack.Pop() + answerStack.Pop());
                        break;
                    case "*":
                        answerStack.Push(answerStack.Pop() * answerStack.Pop());
                        break;
                    case "-":
                        num2 = answerStack.Pop();
                        answerStack.Push(answerStack.Pop() - num2);
                        break;
                    case "/":
                        num2 = answerStack.Pop();
                        if (num2 != 0)
                            answerStack.Push(answerStack.Pop() / num2);
                        else
                            throw new DivideByZeroException("Деление на ноль было обнаружено в ходе вычисления выражения.");
                        break;
                    case "\n":
                        break;
                    default:
                        throw new FormatException("Входная строка имела посторонние символы.");
                }
            }
        }
    }
}
